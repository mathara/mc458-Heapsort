/*
Matheus Mendes Araujo 156737

  Para a resolução do exercício, eu implementei o algoritmo
  utlizando vetor de vetores para armazenar as idades de cada
  espécie, um vetor que terá a maior idade de cada espécie e
  um clone do vetor anterior, no qual realizo o heap para descobrir
  a idade mais velha. O algoritmo busca realizar a operação
  com complexidade O(mlog(k)), m é o número total de árvores e
  k é o número de espécies.
*/
#include <stdio.h>
#include <vector>
#include <iostream>
#include <math.h>
#include <algorithm>
using namespace std;


typedef unsigned long long int utlong;
//variável global para contagem de árvores derrubadas
utlong cont = 0;

void print(vector<int> v){
  for (utlong i = 0; i < v.size(); i++) {
    printf("%d ", v[i] );
  }
  printf("\n");
}

//função para validar se a espécie é jovem
int verificaIdade(vector<int> v, int tam, int maior){
  int flagjovem = 1;
  int velho = 0.6*maior;
  int limite = ceil(0.6*tam) -1;

  //como está em ordem crescente, só é
  //preciso compara a árvore da posiçã limite
  //para definir se é jovem
  if (v[limite] > velho ){
    flagjovem = 0;
    cont++;
  }

  return flagjovem;
}

int main() {
  //variáveis
  int j;
  int maior = 0;
  int flag = 1;
  utlong k_esp = 0;
  utlong size_vet = 0;
  vector< vector <int> > v;
  vector<int> max_aux;
  vector<int> heap;

  //recebe a quantidade de espécies
  cin >> k_esp;
  //cria a floresta
  v.resize(k_esp);

  for (int i = 0; i < k_esp; i++ ){
    //recebe a quantidade de árvores
    cin >> size_vet;
    //redimensiona o vector
    v[i].resize( size_vet );

    //recebe valores da idade
    for (int j = 0; j < size_vet ; j++) {
      cin >> v[i][j];
    }
    //insere em um vetor que terá a maior idade de cada
    // e índice do vetor representa a espécie que essa
    //árvore pertence
     max_aux.push_back(v[i][size_vet-1]);
  }
  //passa o vetor com as maiores idades
  //realiza um heap para descobrir a maior
  heap = max_aux;
  make_heap (heap.begin(),heap.end());
  maior = heap.front();

  do{
    flag = 1;
    // a flag = 1 indica se o cjunto é jovem
    for (j = 0; j < k_esp && flag == 1 ; j++) {
      if ( v[j].size() != 0)
        flag = verificaIdade(v[j], v[j].size(), maior);
    }

    if (flag == 0 ){
      //realiza o corte da árvore
      v[j-1].resize(v[j-1].size() -1);
      //atualiza o vetor de maiores idades
      max_aux[j-1] = v[j-1][v[j-1].size() - 1 ];
      //passa o vetor com as maiores idades
      //realiza um heap para descobrir a maior
      //atualiza maior
      heap = max_aux;
      make_heap (heap.begin(),heap.end());
      maior = heap.front();
    }
  }while (flag == 0);


  //o número de árvores derrubadas
  printf("%llu\n", cont );
}
